# Asher's Wacom Tablet Quick Configuration

The Program is a Linux desktop front-end application designed specifically for the Wacom Drawing tablets. With this application, users can easily configure their Wacom Drawing tablet settings, allowing for a more efficient and personalized drawing experience. The application is user-friendly and easy to navigate, making it simple for users of all skill levels to utilize. Whether you're a beginner or an experienced artist, AsherWacomTabletQuickConfiguration can help streamline your workflow and enhance your digital drawing experience.
You can look around to get an idea how to structure your project and, when done, you can safely delete this project.

There is an accompanying video at: https://youtu.be/BTGjppn4cuQ
This will explain the Options abilities of the Program with the Wacom Drawing Tablet

Depends:
libqt5widgets5 This is a QT5 configuration file
xsetwacom This program is on a front end for this program

You can check us out on social Media at: 
https://www.facebook.com/groups/648648643592658

If you really like the program think about helping out the developer on Patreon at:
https://www.patreon.com/user?u=74944932
