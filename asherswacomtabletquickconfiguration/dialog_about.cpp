#include "dialog_about.h"
#include "ui_dialog_about.h"

Dialog_about::Dialog_about(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_about)
{
    ui->setupUi(this);
    QString labeloutput_title = "Asher's Wacom Drawing Tablet Quick Configuration Tool";
    QString labeloutput_top = "Asher's Wacom Drawing Tablet Quick Configuration Tool\nVersion 1.01\n";
    QString labeloutput_bottom = "The program is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.";

    ui->label_TITLE->setText(labeloutput_title);
    ui->textBrowser_top->setText(labeloutput_top);
    ui->textBrowser_bottom->setText(labeloutput_bottom);
}

Dialog_about::~Dialog_about()
{
    delete ui;
}

void Dialog_about::on_pushButton_quit_clicked()
{
   close();
}

void Dialog_about::on_pushButton_help_video_clicked()
{
    QString output = fire + " https://youtu.be/BTGjppn4cuQ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_about::on_pushButton_facebook_clicked()
{
    QString output = fire + " https://www.facebook.com/groups/648648643592658 &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_about::on_pushButton_qt_clicked()
{
    QString output = fire + "  https://www.qt.io/ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_about::on_pushButton_gpt_clicked()
{
    QString output = fire + "  https://openai.com/ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_about::on_pushButton_sourcecode_clicked()
{
    QString output = fire + "  https://gitlab.com/AshersPrograms/ashers-wacom-tablet-quick-configuration/ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_about::on_pushButton_Helped_qprocess_clicked()
{
    QString output = fire + "  https://gist.github.com/technoburst/ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_about::on_pushButton_mxlinux_clicked()
{
    QString output = fire + " https://mxlinux.org/ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_about::on_pushButton_clicked()
{
    QString output = fire + " https://mxlinux.org/wiki/hardware/wacom/ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}
