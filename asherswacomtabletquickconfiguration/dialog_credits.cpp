#include "dialog_credits.h"
#include "ui_dialog_credits.h"

Dialog_credits::Dialog_credits(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog_credits)
{
    ui->setupUi(this);
}

Dialog_credits::~Dialog_credits()
{
    delete ui;
}

void Dialog_credits::on_pushButton_help_video_clicked()
{
    QString output = fire + " https://youtu.be/BTGjppn4cuQ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_credits::on_pushButton_facebook_clicked()
{
    QString output = fire + " https://www.facebook.com/groups/648648643592658 &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_credits::on_pushButton_qt_clicked()
{
    QString output = fire + "  https://www.qt.io/ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_credits::on_pushButton_gpt_clicked()
{
    QString output = fire + "  https://openai.com/ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_credits::on_pushButton_sourcecode_clicked()
{
    QString output = fire + "  https://gitlab.com/AshersPrograms/ashers-wacom-tablet-quick-configuration/ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_credits::on_pushButton_Helped_qprocess_clicked()
{
    QString output = fire + "  https://gist.github.com/technoburst/5369746 &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_credits::on_pushButton_mxlinux_clicked()
{
    QString output = fire + " https://mxlinux.org/ &";
    QByteArray out = output.toLocal8Bit();
    system(out);
}

void Dialog_credits::on_pushButton_quit_clicked()
{
    close();
}
